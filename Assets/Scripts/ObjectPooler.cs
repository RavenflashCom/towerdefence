﻿using System.Collections.Generic;
using UnityEngine;

namespace TowerDefence
{
    public class ObjectPooler<T> where T : MonoBehaviour
    {
        public List<T> pool { get; private set; }
        T prefab;
        Transform container;

        public ObjectPooler(T prefab, int poolSize)
        {
            this.prefab = prefab;
            InstantiateContainer(prefab.name);
            GeneratePool(poolSize);
        }

        public T PoolObject()
        {
            foreach (T poolObj in pool)
            {
                if (!poolObj.gameObject.activeSelf)
                {
                    poolObj.transform.SetParent(null);
                    poolObj.gameObject.SetActive(true);
                    return poolObj;
                }
            }

            // just in case...
            T t = Object.Instantiate(prefab) as T;
            pool.Add(t);
            return t;
        }

        public void ReturnToPool(T obj)
        {
            obj.gameObject.SetActive(false);
            obj.transform.SetParent(container);
        }

        void InstantiateContainer(string objName)
        {
            container = new GameObject().transform;
            container.name = $"Pool({objName})";
        }

        void GeneratePool(int poolSize)
        {
            pool = new List<T>();

            for (int i = 0; i < poolSize; i++)
            {
                T poolObj = Object.Instantiate(prefab) as T;
                poolObj.gameObject.SetActive(false);
                poolObj.transform.SetParent(container);
                pool.Add(poolObj);
            }
        }

    }
}