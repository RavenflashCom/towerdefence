﻿using UnityEngine;
using UnityEngine.UI;

namespace TowerDefence
{
    [RequireComponent(typeof(Text))]
    public class TowerCounter : MonoBehaviour
    {
        [HideInInspector]
        public Text counter;

        // Use this for initialization
        void Start()
        {
            counter = GetComponent<Text>();

            EventManager.OnTowerCounterUpdate += DisplayValue;
        }

        public void DisplayValue(int num)
        {
            counter.text = "Wieżyczki: "+num;
            if (num > 100) Debug.LogError("THIS IS TO MUCH: " + num);
        }

    }

}