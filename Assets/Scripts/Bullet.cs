﻿using UnityEngine;

namespace TowerDefence {
    public class Bullet : MonoBehaviour {

        //constants
        const float BulletSpeed = 4f;

        //properties
        public Tower origin { get; private set; }

        //fields
        float bulletRange;
        float distanceTraveled;

        //methods
        public void SetPositionAndRange(Tower origin, float bulletRange)
        {
            this.origin = origin;
            this.bulletRange = bulletRange;
            transform.position = origin.transform.position;
            transform.rotation = origin.transform.rotation;
            distanceTraveled = 0;
        }

        public void OnCollisionEnter2D(Collision2D collision)
        {
            Tower t = collision.gameObject.GetComponent<Tower>();
            if (!t) return;
            if (origin == t) return;

            EventManager.BulletDestroyed(this);
        }

        internal void UpdateBullet()
        {
            Vector3 moveVector = transform.up * BulletSpeed * Time.fixedDeltaTime;
            transform.position += moveVector;
            distanceTraveled += moveVector.magnitude;

            if (distanceTraveled >= bulletRange)
            {
                EventManager.BulletLanded(transform.position);
                EventManager.BulletDestroyed(this);
            }
        }

    }
}