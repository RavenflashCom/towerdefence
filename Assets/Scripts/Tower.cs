﻿using System.Collections;
using UnityEngine;

namespace TowerDefence
{
    public class Tower : MonoBehaviour
    {
        //constants
        const float MinRotation = 15f;
        const float MaxRotation = 45f;
        const float RotationPerion = .5f;
        const int MaxNumberOfShots = 12;
        const float FirstShotDelay = 6f;

        // fields
        SpriteRenderer spriteRenderer;
        int shotCount = 0;

        Coroutine initialCoroutine, 
                  rotateCoroutine;

        //methods
        public void Awake()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
        }
        
        public void GetReadyToShoot()
        {
            shotCount = 0;
            gameObject.SetActive(true);
            initialCoroutine = StartCoroutine(DelayedStartShooting());
        }

        public void StartShooting()
        {
            spriteRenderer.color = Color.red;
            rotateCoroutine = StartCoroutine(RotateByRandom());
        }

        public void RestartShooting()
        {
            StopShooting();
            shotCount = 0;
            StartShooting();
        }

        public void OnCollisionEnter2D(Collision2D collision)
        {
            Bullet b = collision.gameObject.GetComponent<Bullet>();
            if (!b) return;
            if (b.origin == this) return;

            StopShooting();
            EventManager.TowerDestroyed(this);
        }

        void StopShooting()
        {
            if(initialCoroutine != null)
                StopCoroutine(initialCoroutine);
            if(rotateCoroutine != null)
                StopCoroutine(rotateCoroutine);

            spriteRenderer.color = Color.white;
        }

        void FireBullet()
        {
            float range = Random.Range(1f, 4f);
            Bullet b = AppManager.instance.bulletPooler.PoolObject();
            if(b) b.SetPositionAndRange(this, range);
            shotCount++;
        }

        #region COROUTINES
        IEnumerator DelayedStartShooting()
        {
            yield return new WaitForSeconds(FirstShotDelay);
            StartShooting();
        }

        IEnumerator RotateByRandom()
        {
            while (true)
            {
                float randomRotation = Random.Range(MinRotation, MaxRotation);
                transform.Rotate(Vector3.back, randomRotation);
                FireBullet();

                if (shotCount >= MaxNumberOfShots)
                {
                    StopShooting();
                    break;
                }
                yield return new WaitForSeconds(RotationPerion);
            }

        }
        #endregion

    }
}