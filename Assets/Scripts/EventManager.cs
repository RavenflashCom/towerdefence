﻿using System;
using UnityEngine;

namespace TowerDefence {
    public static class EventManager
    {
        // Tower events
        public static event Action<Tower> OnTowerDestroyed;
        public static event Action<int> OnTowerCounterUpdate;

        // Bullet events
        public static event Action<Vector3> OnBulletLanded;
        public static event Action<Bullet> OnBulletDestroyed;

        public static void TowerDestroyed(Tower tower) { OnTowerDestroyed?.Invoke(tower); }
        public static void TowerCounterUpdate(int value) { OnTowerCounterUpdate?.Invoke(value); }
        public static void BulletLanded(Vector3 position) { OnBulletLanded?.Invoke(position); }
        public static void BulletDestroyed(Bullet bullet) { OnBulletDestroyed?.Invoke(bullet); }
    }
}