﻿using System;
using UnityEngine;

namespace TowerDefence
{
    public class AppManager : MonoBehaviour
    {
        // constants
        const int TowerLimit = 100; // also pool size
        const int BulletPoolSize = 150;

        //fields        
        public static AppManager instance;
        internal ObjectPooler<Bullet> bulletPooler;
        internal ObjectPooler<Tower> towerPooler;
        internal int towerCount = 0;

        //prefabs
        [SerializeField]
        Tower towerPrefab = null;
        [SerializeField]
        Bullet bulletPrefab = null;

        //methods
        public void Awake()
        {
            instance = this;
            towerPooler = new ObjectPooler<Tower>(towerPrefab, TowerLimit);
            bulletPooler = new ObjectPooler<Bullet>(bulletPrefab, BulletPoolSize);

            EventManager.OnBulletDestroyed += DespawnBullet;
            EventManager.OnTowerDestroyed += DespawnTower;
            EventManager.OnBulletLanded += SpawnTowerAt;
        }

        void Start()
        {
            Tower firstTower = SpawnTower();
            firstTower.StartShooting();
        }

        void FixedUpdate()
        {
            UpdateAllBullets();
        }

        void UpdateAllBullets()
        {
            foreach(Bullet b in bulletPooler.pool)
            {
                if(b.gameObject.activeSelf)
                    b.UpdateBullet();
            }
        }

        void OnDestroy()
        {
            EventManager.OnBulletDestroyed -= DespawnBullet;
            EventManager.OnBulletLanded -= SpawnTowerAt;
            EventManager.OnTowerDestroyed -= DespawnTower;
        }

        internal void SpawnTowerAt(Vector3 position)
        {
            Tower t = SpawnTower();
            t.transform.position = position;
            if (towerCount >= TowerLimit) FireAllGuns();
            t.GetReadyToShoot();
        }

        Tower SpawnTower()
        {
            Tower t = towerPooler.PoolObject();
            towerCount++;
            EventManager.TowerCounterUpdate(towerCount);
            return t;
        }

        void DespawnTower(Tower tower)
        {
            towerCount--;
            EventManager.TowerCounterUpdate(towerCount);
            towerPooler.ReturnToPool(tower);
        }

        void FireAllGuns()
        {
            EventManager.OnBulletLanded -= SpawnTowerAt;
            foreach (Tower t in towerPooler.pool)
            {
                if (t.gameObject.activeSelf) t.RestartShooting();
            }
        }

        void DespawnBullet(Bullet bullet)
        {
            bulletPooler.ReturnToPool(bullet);
        }

    }
}