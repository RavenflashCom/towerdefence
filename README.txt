1. Po uruchomieniu gry na scenie powinna być jedna wieżyczka.
2. Wieżyczka obraca się co 0.5 sekundy o random (15,45) stopni.
3. Po obrocie wieżyczka wystrzeliwuje pocisk, który leci random (1,4) jednostek.
4. Pocisk na końcu swej drogi jest niszczony i w jego miejscu pojawia się nowa wieżyczka (nie obrócona), która czeka 6 sekund, po czym przyjmuje zachowanie pierwszej wieżyczki.
5. Prędkość pocisku powinna wynosić 4 jednostki/sekundę.
6. Jeśli pocisk uderzy w wieżyczkę to zarówno pocisk jak i wieżyczka zostają zniszczone (pocisk nie tworzy wtedy nowej wieżyczki).
7. Każda wieżyczka strzela 12 razy, po czym przestaje się obracać i strzelać.
8. Obiekt aktywnej wieżyczki (która obraca się i strzela) ma kolor czerwony RGB (255,0,0).
9. Obiekt nieaktywnej wieżyczki (która czeka) ma kolor naturalny RGB (255,255,255).
10. Jeśli istnieje już 100 wieżyczek to wszystkie wieżyczki zaczynają znowu strzelać i obracać się 12 razy, ale nowe wieżyczki nie są już tworzone.
11. Na scenie powinien być napis "Wieżyczki: x", gdzie x jest ilością aktualnie istniejących wieżyczek.